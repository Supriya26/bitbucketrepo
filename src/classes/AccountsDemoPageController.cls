public class AccountsDemoPageController {
	public List<Account> accounts {get;set;}
	
	public AccountsDemoPageController(){
		
		accounts = new List<Account>();
		accounts = AccountService.getAccounts();		
	}

}